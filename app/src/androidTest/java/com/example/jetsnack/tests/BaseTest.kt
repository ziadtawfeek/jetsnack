package com.example.jetsnack.tests

import android.content.Context
import androidx.compose.ui.test.junit4.createEmptyComposeRule
import androidx.test.platform.app.InstrumentationRegistry
import com.example.jetsnack.model.snacks
import org.junit.Rule

abstract class BaseTest {
    private fun getComposeTestActivity() = createEmptyComposeRule()

    val targetContext: Context = InstrumentationRegistry.getInstrumentation().targetContext

    @get:Rule
    open var composeTestRule = this.getComposeTestActivity()

    @JvmField
    val randomSnackId = snacks.random().id
}