/*
 * Copyright 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.jetsnack.tests

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertTextContains
import androidx.compose.ui.test.junit4.ComposeContentTestRule
import androidx.compose.ui.test.junit4.ComposeTestRule
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithText
import com.example.jetsnack.R
import com.example.jetsnack.model.OrderLine
import com.example.jetsnack.model.SnackRepo
import com.example.jetsnack.ui.home.cart.Cart
import com.example.jetsnack.ui.theme.JetsnackTheme
import com.example.jetsnack.ui.utils.formatPrice
import org.junit.Before
import org.junit.Test

class CartScreenTests : BaseTest() {

    private val snackName = SnackRepo.getSnack(randomSnackId).name
    private val snackPrice = SnackRepo.getSnack(randomSnackId).price
    private val snackTagline = SnackRepo.getSnack(randomSnackId).tagline
    override var composeTestRule: ComposeTestRule = createComposeRule()

    @Before
    fun setUp() {
        (composeTestRule as ComposeContentTestRule).setContent {
            JetsnackTheme {
                Cart(
                    orderLines = listOf(
                        OrderLine(
                            snack = SnackRepo.getSnack(randomSnackId), 1
                        )
                    ),
                    removeSnack = { },
                    increaseItemCount = { },
                    decreaseItemCount = { },
                    inspiredByCart = SnackRepo.getInspiredByCart(),
                    onSnackClick = {}
                )
            }
        }
        composeTestRule.mainClock.autoAdvance = false
    }

    @Test
    fun verify_SingleOrderItemInCartScreen_DisplayedCorrectly() {
        composeTestRule
            .onNodeWithText("Order (1 item)")
            .assertIsDisplayed()
    }

    @Test
    fun verify_NameInCartScreen_DisplayedCorrectly() {
        composeTestRule
            .onNodeWithText(snackName)
            .assertIsDisplayed()
    }

    @Test
    fun verify_PriceInCartScreen_DisplayedCorrectly() {
        composeTestRule
            .onNodeWithText(snackName)
            .assertTextContains(formatPrice(snackPrice))
            .assertIsDisplayed()
    }

    @Test
    fun verify_TaglineInCartScreen_DisplayedCorrectly() {
        composeTestRule
            .onNodeWithText(snackName)
            .assertTextContains(snackTagline)
            .assertIsDisplayed()
    }

    @Test
    fun verify_RemoveItemButtonDisplayed_InCartScreen() {
        composeTestRule
            .onNodeWithText(snackName)
            .assertTextContains(snackName)
            .assertIsDisplayed()

        composeTestRule
            .onNodeWithContentDescription(
                targetContext.getString(R.string.label_remove), ignoreCase = true
            )
            .assertIsDisplayed()
    }
}
