/*
 * Copyright 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.jetsnack.tests

import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.ComposeContentTestRule
import androidx.compose.ui.test.junit4.ComposeTestRule
import androidx.compose.ui.test.junit4.createComposeRule
import com.example.jetsnack.R
import com.example.jetsnack.model.SnackRepo
import com.example.jetsnack.ui.snackdetail.SnackDetail
import com.example.jetsnack.ui.theme.JetsnackTheme
import com.example.jetsnack.ui.utils.formatPrice
import org.junit.Before
import org.junit.Test

class ProductDetailsScreenTests : BaseTest() {

    private val snackName = SnackRepo.getSnack(randomSnackId).name
    private val snackPrice = SnackRepo.getSnack(randomSnackId).price
    private val snackTagline = SnackRepo.getSnack(randomSnackId).tagline
    override var composeTestRule: ComposeTestRule = createComposeRule()

    @Before
    fun setUp() {
        (composeTestRule as ComposeContentTestRule).setContent {
            JetsnackTheme {
                SnackDetail(snackId = randomSnackId) {}
            }
        }
        composeTestRule.mainClock.autoAdvance = false
    }

    @Test
    fun verify_NameInSnackDetailScreen_DisplayedCorrectly() {
        composeTestRule
            .onNodeWithText(snackName)
            .assertIsDisplayed()
    }

    @Test
    fun verify_PriceInSnackDetailsScreen_DisplayedCorrectly() {
        composeTestRule
            .onNodeWithText(formatPrice(snackPrice))
            .assertTextEquals(formatPrice(snackPrice))
            .assertIsDisplayed()
    }

    @Test
    fun verify_TaglineInSnackDetailsScreen_DisplayedCorrectly() {
        composeTestRule
            .onNodeWithText(snackTagline)
            .assertTextEquals(snackTagline)
            .assertIsDisplayed()
    }

    @Test
    fun verify_SnackDescriptionInSnackDetailsScreen_DisplayedCorrectly() {
        composeTestRule.onRoot().printToLog("composableLog")
        composeTestRule
            .onNodeWithText(
                targetContext.getString(R.string.detail_placeholder)
            )
            .assertIsDisplayed()
    }

    @Test
    fun verify_IngredientsDisplayedInClickingSeeMore_InSnackDetailsScreen() {
        composeTestRule
            .onNodeWithText(
                targetContext.getString(R.string.ingredients)
            )
            .assertIsDisplayed()
    }
}
