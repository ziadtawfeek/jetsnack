package com.example.jetsnack.tests

import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    CartScreenTests::class,
    ProductDetailsScreenTests::class
)
class TestRunnerClass